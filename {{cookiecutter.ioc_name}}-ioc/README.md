# {{ cookiecutter.ioc_name }}

{{ cookiecutter.summary }}

=======

## IOC template

This project was generated with the [E3 cookiecutter IOC template](https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-ioc).