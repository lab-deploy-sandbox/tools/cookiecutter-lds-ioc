import re
import sys

IOC_NAME_REGEX = r'^[A-Za-z0-9_-]*$'

ioc_name = '{{ cookiecutter.ioc_name }}'

if not re.match(IOC_NAME_REGEX, ioc_name):
    print('ERROR: "{}" is not a valid IOC name! It should match "^[A-Za-z0-9_-]*$"'.format(ioc_name))
    sys.exit(1)

def main():
    {% if cookiecutter.dependencies  == 'nfs + cell'%}
        {{cookiecutter.update({"module_dir": "$(essioc_DIR)/common_config.iocsh"})}}
        {{cookiecutter.update({"require_mod": "require essioc"})}}
    {% elif cookiecutter.dependencies  == 'conda'%}
        {{cookiecutter.update({"module_dir": "$(essioc_DIR)/essioc.iocsh"})}}
        {{cookiecutter.update({"require_mod": "#require mymodule"})}}
    {% endif %}
        

if __name__ == '__main__':
    main()

