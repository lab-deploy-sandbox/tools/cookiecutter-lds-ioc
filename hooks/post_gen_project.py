import sys
import shutil
import os

def remove_dir(dirname):
    if os.path.isdir(dirname):
        shutil.rmtree(dirname)
    else:
        print("ERROR: directory '{}' can not be deleted.".format(dirname))
        sys.exit(1)

def main():
    if '{{ cookiecutter.custom_db_files }}' == 'N':
        remove_dir("db")

    if '{{ cookiecutter.custom_env_files }}' == 'N':
        remove_dir("envs")

    if '{{ cookiecutter.custom_iocsh_files }}' == 'N':
        remove_dir("iocsh")

    if '{{ cookiecutter.dependencies }}' == 'nfs + cell':
        os.remove("environment.yaml")
    elif '{{ cookiecutter.dependencies }}' == 'conda':
        os.remove("cell.yml")
        os.remove("env.sh")
    else: 
        print("{{ cookiecutter }}")

if __name__ == '__main__':
    main()
