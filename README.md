# e3 IOC cookiecutter template

[Cookiecutter](https://github.com/audreyr/cookiecutter) template for LDS IOCs.

## Quickstart

Install the latest Cookiecutter if you haven't installed it yet:

```
$ pip install cookiecutter
```

Generate an EPICS/E3 IOC for use with LDS:

```
$ cookiecutter git+https://gitlab.esss.lu.se/lab-deploy-sandbox/tools/cookiecutter-lds-ioc.git
```

As this is not easy to remember, you can add an alias in your `~/.bash_profile`:

```
alias lds-ioc='cookiecutter git+https://gitlab.esss.lu.se/lab-deploy-sandbox/tools/cookiecutter-lds-ioc.git'
```

## Modules

The startup script loads the standard ESS EPICS modules using the [essioc](https://gitlab.esss.lu.se/e3-recipes/essioc-recipe) package:

```
iocshLoad("$(essioc_DIR)/essioc.iocsh")
```

The **essioc** package includes:
* epics-base
* require
* iocStats
* recsync
* caPutLog
* ess
* autosave

Those modules shall not be defined in the `st.cmd` file (nor in the `environment.yaml`).

The modules required by the IOC shall be added to the startup script (without version):

```
require mymodule
```

The modules to install shall also be added to the `environment.yaml` file:

```
dependencies:
  - essioc
  - mymodule=1.0.0
```

## Databases

Local databases for this IOC should be stored in the `db` directory. Instructions to load the database should be included in the startup script.
